#### React Tutorial
- JavaScript library
- create reusable UI components

#### Create React App
- set up a React Environment

- uses the create-react-app.
    - npm install -g create-react-app
    - set up everything you need to run a React application
    
- The create-react-app is an officially supported way to create React applications.

- create a React application named myfirstreact
    - npx create-react-app myfirstreact
    
- Created git commit.

- Inside that directory, you can run several commands:
  
  yarn start
    Starts the development server.

  yarn build
    Bundles the app into static files for production.

  yarn test
    Starts the test runner.

  yarn eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

- We suggest that you begin by typing:

  cd myfirstreact
  yarn start

    
#### Run the React Application

- run your first real React application
    - cd myfirstreact
    
    - execute the React application myfirstreact
        - npm start
        - http://localhost:3000/
    
#### What You Should Already Know

- HTML
- CSS
- JavaScript
- ECMAScript 6 (ES6)

#### How does React Work?

- React creates a VIRTUAL DOM in memory.
- it does all the necessary manipulating, before making the changes in the browser DOM.

- React only changes what needs to be changed!


#### React.JS History


#### React Getting Started
- you need NPM and Node.js


#### React Directly in HTML
- first two write React code in our JavaScripts
- third, Babel, allows us to write JSX syntax and ES6 in older browsers.


      <script src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
      <script src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
      <script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>
      
      <script type="text/babel">
          class Hello extends React.Component {
            render() {
              return <h1>Hello World!</h1>
            }
          }
      </script>

      ReactDOM.render(<Hello />, document.getElementById('mydiv'))
  
- using React can be OK for testing purposes, 
- for production you need to set up a React environment.


#### Setting up a React Environment


#### Modify the React Application

- how do I change the content
- Look in the myfirstreact directory,
- find a src folder
- file called App.js
    
    
    vim /myfirstreact/src/App.js:
    

